import MyCourse from "./components/MyCourse";
import MySetting from "./components/MySetting";
import CourseManage from "./components/CourseManage";

let util={};
util.title="虛實自學協助平台";
util.role={
    'Student': '學生',
    'Teacher': '老師',
    'Provider': '教材提供者',
    'Manager': '管理者'
}
util.courseType={
    "HoC": "一小時程式設計",
    "basic": "基礎程式設計",
    "advanced": "進階程式設計"
};
util.chapterType={
    "text": "一般多媒體文章",
    "implement": "實作練習"
}
util.components={
    'MyCourse': <MyCourse/>,
    'MySetting': <MySetting/>,
    'CourseManage': <CourseManage/>
};

util.example={
    blockDef:{
        type: "example",
        message0: "測試方塊",
        previousStatement: null,
        nextStatement: null,
        colour: 160,
        tooltip: "",
        helpUrl: ""
    },
    blockCodeDef: "var code = '...;\\n';"
}

util.getTitleByPrefix=(prefix)=>{
    return `${prefix} - ${util.title}`;
}
util.backend={
    host: "desktop-pcchenbackend/project/public/api"
};

util.login=async (formData)=>{
    const json=await fetch(`https://${util.backend.host}/auth/login`,{
        method: 'post',
        body: formData,
        credentials: 'include'
    }).then(response=>response.json());
    return json;
}

util.logout=async ()=>{
    const json=await fetch(`https://${util.backend.host}/auth/logout`,{
        method: 'post',
        credentials: 'include'
    }).then(response=>response.json());
    return json;
}

util.getMe=async ()=>{
    const json=await fetch(`https://${util.backend.host}/auth`,
    {
        method: 'get',
        credentials: 'include'
    }).then(response=>response.json());
    return json;
}

util.addCourse=async (formData)=>{
    const json=await fetch(`https://${util.backend.host}/courses`,
    {
        method: 'post',
        body: formData,
        credentials: 'include'
    }).then(response=>response.json());
    return json;
}

util.editCourse=async (data,id)=>{
    const json=await fetch(`https://${util.backend.host}/courses/${id}`,
    {
        method: "put",
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        },
        body: JSON.stringify(data),
        credentials: "include"
    }).then(response=>response.json());
    return json;
}

util.getCourses=async ()=>{
    const json=await fetch(`https://${util.backend.host}/courses`,
    {
        method: 'get',
        credentials: 'include'
    }).then(response=>response.json());
    return json;
}

util.getCourse=async (id)=>{
    const json=await fetch(`https://${util.backend.host}/courses/${id}`,
    {
        method: "get",
        credentials: "include"
    }).then(response=>response.json());
    return json;
}

export default util;