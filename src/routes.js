import LoginPage from './pages/LoginPage';
import WelcomePage from './pages/WelcomePage';
import DashboardPage from './pages/DashboardPage';

let routes=[
    {
        path: '/',
        component: <WelcomePage/>
    },
    {
        path: '/login',
        component: <LoginPage/>
    },
    {
        path: '/dashboard',
        component: <DashboardPage/>
    }

]

export default routes;