import React from 'react';
import EditorJS from '@editorjs/editorjs';
import util from '../util';

class CourseForm extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state={
            editor: null,
            editorId: (props.course==null)?"add":`editor-${props.course.id}`,
            submit: (props.course==null)?this.addCourse:this.editCourse,
            course: (props.course==null)?{}:JSON.parse(JSON.stringify(props.course))
        };
    }
    handleInputChange=(e)=>
    {
        let element=e.target;
        let value=element.value;
        let name=element.name;
        let newCourse=this.state.course;
        newCourse[name]=value;
        this.setState({course: newCourse});
    }
    addCourse=(e)=>
    {
        e.preventDefault();
        let formData=new FormData(e.target);
        this.state.editor.save()
            .then(result=>{
                formData.append("desc",JSON.stringify(result));
                util.addCourse(formData)
                    .then(json=>{
                        console.log(json);
                    });
            });
    }
    editCourse=(e)=>
    {
        e.preventDefault();
        let formData=new FormData(e.target);
        this.state.editor.save()
            .then(result=>{
                formData.append("desc",JSON.stringify(result));
                let data={};
                formData.forEach((value,key)=>data[key]=value);
                util.editCourse(data,this.props.course.id)
                    .then(json=>{
                        this.props.update();
                        return new Promise(resolve=>setTimeout(resolve(json),500));
                    })
                    .then(json=>{
                        util.getCourse(json.id)
                            .then(json=>{
                                this.setState({course: json})
                            });
                    })
            });
    }
    componentDidMount()
    {
        this.setState({editor: new EditorJS({
            holder: this.state.editorId,
            data: (this.state.editorId==='add')?{}:JSON.parse(this.props.course.desc)
        })},()=>
        {
            let editor=this.state.editor;
            editor.isReady.then(()=>{
                this.componentDidUpdate=(prevProps)=>{
                    if(this.props!==prevProps)
                    {
                        if(this.state.editorId!=="add")
                        {
                            editor.blocks.clear();
                            let json=JSON.parse(this.props.course.desc);
                            if(json.blocks.length!==0)
                                editor.blocks.render(json);
                        }
                        if(this.props.course!=null)
                            this.setState({course: JSON.parse(JSON.stringify(this.props.course))});
                    }
                }
            });
        });
    }
    render()
    {
        let course=this.state.course;
        if(course==null)
        {
            course={
                name: "",
                type: "HoC",
                min_age: 0,
                max_age: 100,
                intro: ""
            }
        }
        return (
            <form className={`${this.props.className} p-4`} onSubmit={this.state.submit}>
                <div className='input-group mb-3'>
                    <span className='input-group-text'>課程名稱</span>
                    <input type="text" className="form-control" placeholder='請輸入課程名稱(必填)' name="name" value={course.name} onChange={this.handleInputChange}/>
                    <span className='input-group-text'>課程分類</span>
                    <select className="form-select" placeholder='請選擇課程分類' name="type" value={course.type} onChange={this.handleInputChange}>
                        <option value="HoC">一小時程式設計(Hour of Code)</option>
                        <option value="basic">基礎程式設計</option>
                        <option value="advanced">進階程式設計</option>
                    </select>
                </div>
                <div className='input-group mb-3'>
                    <span className='input-group-text' >建議年齡</span>
                    <input type="number" className='form-control' value={course.min_age} name="min_age" onChange={this.handleInputChange}/>
                    <span className='input-group-text'>~</span>
                    <input type="number" className='form-control' value={course.max_age} name="max_age" onChange={this.handleInputChange}/>
                </div>
                <div className='input-group'>
                    <span className='input-group-text'>課程簡介</span>
                    <textarea className="form-control" name="intro" value={course.intro} onChange={this.handleInputChange}></textarea>
                </div>
                <div className='input-group mb-3'>
                    <span className='input-group-text'>課程說明</span>
                    <div id={this.state.editorId} className="form-control"></div>
                </div>
                <div className='d-grid'>
                    <button className='btn btn-primary'>送出</button>
                </div>
            </form>
        )
    }
}

export default CourseForm;
