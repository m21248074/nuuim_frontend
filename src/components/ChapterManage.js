import React from 'react';
import ChapterForm from './ChapterForm';

class ChapterManage extends React.Component
{
    render()
    {
        let course=this.props.course;
        return (
                    <div className='row'>
                        {/* sidebar */}
                        <div className='col vh-80 m-4'>
                            <div className='d-grid'>
                                <button className='btn btn-primary'>新增章節</button>
                                {/* collpase */}
                            </div>
                            <ul className='nav nav-pills nav-fill flex-column mb-auto border'>
                                <li className='nav-item'>aaa</li>
                            </ul>
                        </div>
                        <div className='col-10'>
                            <ChapterForm course={course}/>
                        </div>
                    </div>
            // chapter edit
        );
    }
}

export default ChapterManage;