import React from 'react';
import util from '../util';
import EditorJS from '@editorjs/editorjs';

class ChapterForm extends React.Component
{
    constructor(props)
    {
        super(props)
        this.state={
            editor: null,
            editorId: (props.chapter==null)?"addChapter":`editor-${props.chapter.id}`
        }
    }
    componentDidMount()
    {
        this.setState({editor: new EditorJS({
            holder: this.state.editorId,
            data: (this.state.editorId==='addChapter')?{}:{}
        })})
    }
    render()
    {
        return (
            <form className='p-4'>
                <div className='input-group mb-3'>
                    <span className='input-group-text'>章節名稱</span>
                    <input className='form-control' placeholder='請輸入章節名稱(必填)'/>
                    <span className='input-group-text'>章節類型</span>
                    <select className='form-select'>
                        {Object.entries(util.chapterType).map(value=>{
                            return <option key={value[0]} value={value[0]}>{value[1]}</option>
                        })}
                    </select>
                </div>
                <div className='input-group mb-3'>
                    <span className='input-group-text'>章節說明</span>
                    <div className='form-control' id={this.state.editorId}></div>
                </div>
                <div className='input-group mb-3'>
                    <span className='input-group-text'>額外積木方塊</span>
                    <div className="form-control"></div>
                </div>
                <div className='input-group mb-3'>
                    <span className='input-group-text'>範例程式</span>
                    <div className='form-control'></div>
                    <span className='input-group-text'>參考答案</span>
                    <div className='form-control'></div>
                </div>
                <div className='d-grid'>
                    <button className='btn btn-primary'>送出</button>
                </div>
            </form>
        );
    }

}

export default ChapterForm;