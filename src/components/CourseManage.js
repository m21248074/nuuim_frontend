import React from 'react';
import util from '../util';
import ChapterManage from './ChapterManage';
import CourseAdvancedForm from './CourseAdvancedForm';
import CourseCard from "./CourseCard";
import CourseForm from './CourseForm';

class CourseManage extends React.Component
{
    constructor()
    {
        super();
        this.state={
            editor: null,
            courses: [],
            currentCourse: null,
            setCurrentCourse: (currentCourse)=>{this.setState({currentCourse})}
        }
    }
    handleClick=(e)=>
    {
        util.getCourses()
            .then(json=>{
                this.setState({
                        courses: json
                    });
            });
    }
    addCourse=(e)=>
    {
        e.preventDefault();
        let formData=new FormData(e.target);
        this.state.editor.save()
            .then(result=>{
                formData.append("desc",JSON.stringify(result));
                util.addCourse(formData)
                    .then(json=>{
                        console.log(json);
                    });
            });
    }
    componentDidMount()
    {
        
    }
    render()
    {
        let courses=this.state.courses;
        let isCoursesEmpty=courses.length===0;
        let coursesCard=null;
        if(isCoursesEmpty)
            coursesCard="無任何課程";
        else
            coursesCard=courses.map(course=>{
                return (
                    <div key={course.id} className="col">
                        <div className="card">
                            <CourseCard course={course} update={this.handleClick} setCourse={this.state.setCurrentCourse}/>
                        </div>
                    </div>
                )
            });
        let course=this.state.currentCourse;
        let currentCourse;
        if(course==null)
            currentCourse="";
        else
        {
            currentCourse=(
                <div className='modal-content'>
                    <div className='modal-header'>
                        <h5 className='modal-title'>編輯教材 - {course.name}</h5>
                        <button className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className='modal-body'>
                        <ul className='nav nav-tabs nav-fill'>
                            <li className='nav-item' role="presentation">
                                <button className='nav-link active' data-bs-toggle="tab" data-bs-target="#editChapterInfo">編輯基本資訊</button>
                            </li>
                            <li className='nav-item' role="presentation">
                                <button className='nav-link' data-bs-toggle="tab" data-bs-target="#chapterManage">章節管理</button>
                            </li>
                        </ul>
                        <div className='tab-content border'>
                            <div className="tab-pane fade show active" id="editChapterInfo">
                                <div className='row'>
                                    <div className='col'>
                                        <CourseForm course={course} update={this.handleClick}/>
                                    </div>
                                    <div className='col'>
                                        <CourseAdvancedForm course={course}/>
                                    </div>
                                </div>
                            </div>
                            <div className='tab-pane fade' id="chapterManage">
                                <ChapterManage course={course}/>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
        return (
            <div className='container border mt-4'>
                <ul className='nav nav-tabs nav-fill mt-4' role="tablist">
                    <li className='nav-item' role="presentation">
                        <button className="nav-link active" data-bs-toggle="tab" data-bs-target="#createClass">新增課程</button>
                    </li>
                    <li className='nav-item' role="presentation">
                        <button className="nav-link" data-bs-toggle="tab" data-bs-target="#manageClass" onClick={this.handleClick}>管理課程</button>
                    </li>
                </ul>
                <div className='tab-content'>
                    <div className="tab-pane fade show active" id="createClass">
                        <CourseForm className="border"/>
                    </div>
                    <div className="tab-pane fade" id="manageClass">
                        <div className='container border pt-4 pb-4'>
                            <div className="row row-cols-1 row-cols-md-2 g-4">
                                {coursesCard}
                            </div>
                        </div>
                    </div>
                </div>
                {/* modal */}
                <div className="modal fade" id="editCourseModal">
                    <div className='modal-dialog modal-fullscreen'>
                        {currentCourse}
                    </div>
                </div>
            </div>
        );
    }
}

export default CourseManage;