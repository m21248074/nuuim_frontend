import React from 'react';
import util from "../util";

class CourseCard extends React.Component
{
    editClick=(e)=>{
        this.props.setCourse(this.props.course);
    }
    render()
    {
        let course=this.props.course;
        return (
            <React.Fragment>
                <div className='card-body'>
                    <h5 className='card-title'>{course.name}</h5>
                    <p className='card-text'>{course.intro}</p>
                </div>
                <ul className='list-group list-group-flush'>
                    <li className='list-group-item'>教材類型: {util.courseType[course.type]}</li>
                    <li className='list-group-item'>適合年齡: {`${course.min_age}歲 ~ ${course.max_age}歲`}</li>
                </ul>
                <button className='btn btn-primary' data-bs-toggle="modal" data-bs-target="#editCourseModal" onClick={this.editClick}>編輯</button>
            </React.Fragment>
        );
    }
}

export default CourseCard;