import React from "react";
import { HashLink } from "react-router-hash-link";

class Footer extends React.Component
{
    render()
    {
        return (
            <footer className="py-3 my-4">
                <ul className="nav justify-content-center border-bottom pb-3 mb-3">
                    <li className="nav-item">
                        <a href="/" className="nav-link px-2 text-muted">首頁</a>
                    </li>
                    <li className="nav-item">
                        <HashLink to="/#features" className="nav-link px-2 text-muted">系統特色</HashLink>
                    </li>
                    <li className="nav-item">
                        <HashLink to="/#users" className="nav-link px-2 text-muted">系統使用對象</HashLink>
                    </li>
                </ul>
                <p className="text-center text-muted">Copyright © 2022 田凱維、莊順程、賴佑承、張詠翔、秦靖哲、劉家緣</p>
            </footer>
        )
    }
}

export default Footer;