import React from 'react';
import Editor from '@monaco-editor/react';

import * as Blockly from 'blockly/core';
import util from '../util';

class CourseAdvancedForm extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state={
            editor: null,
            blockEditor: null,
            blockDefEditor: null,
            blockly: null,
            currentLink: '',
            fileLinks: [],
            blocks: {}
        };
    }
    addLink=(e)=>{
        this.setState({fileLinks: [...this.state.fileLinks,this.state.currentLink]});
    }
    deleteLink=(link)=>{
        this.setState({fileLinks: this.state.fileLinks.filter((l)=>{
            return l!==link;
        })});
    }
    currentLinkChange=(e)=>{
        this.setState({currentLink: e.target.value});
    }
    handleSubmit=(e)=>{
        console.log("submit");
        // e.preventDefault();
        // console.log(this.state.fileLinks);
        // console.log(this.state.editor.getValue());
    }
    handleEditorDidMount=(editor,monaco)=>{
        this.setState({editor: editor});
    }
    handleBlockEditorDidMount=(editor,monaco)=>{
        this.setState({blockEditor:editor});
    }
    handleBlockDefEditorDidMount=(editor,monaco)=>{
        this.setState({blockDefEditor: editor});
    }
    addBlock=()=>{
        let block=JSON.parse(this.state.blockEditor.getValue());
        let code=this.state.blockDefEditor.getValue();
        Blockly.Blocks[block.type]={
            init: function(){
                this.jsonInit(block);
            }
        }
        Blockly.JavaScript[block.type]=function(block){
            //eval();
            return code;
        }
        console.log(Blockly);
        Blockly.serialization.blocks.append({'type':block.type},this.state.blockly);
    }
    initBlockly()
    {
        let blockly=Blockly.inject(`shareBlock`,
            {
                grid:
                {
                    spacing: 20,
                    length: 3,
                    colour: "#ccc",
                    snap: true
                },
                trashcan: true
            }
        );
        function onDeleteBlock(event)
        {
            if(event instanceof Blockly.Events.BLOCK_DELETE)
            {
                console.log(event);
            }
        }
        blockly.addChangeListener(onDeleteBlock);
        return blockly;
    }
    componentDidMount()
    {
        if(!this.isAdvancedClass())
        {
            this.setState({blockly: this.initBlockly()});
        }
    }
    componentDidUpdate(prevProps,prevState)
    {
        if(!this.isAdvancedClass()&&this.state.blockly==null)
        {
            this.setState({blockly: this.initBlockly()});
        }
    }
    isAdvancedClass=()=>
    {
        return this.props.course.type==="advanced";
    }
    render()
    {
        let fileLinks=this.state.fileLinks;
        let isLinksEmpty=fileLinks.length===0;
        let linkItems=null;
        if(isLinksEmpty)
            linkItems=<div className='list-group-item'>目前沒有任何連結</div>;
        else
        {
            linkItems=fileLinks.map(link=>{
                return (
                    <div key={link} className='list-group-item'>
                        <div className='input-group'>
                            <span className='input-group-text form-control'>{link}</span>
                            <button className='btn btn-primary' onClick={
                                ()=>this.deleteLink(link)
                            }>刪除</button>
                        </div>
                    </div>
                )
            })
        }
        let displayBlockly="";
        if(!this.isAdvancedClass())
        {
            displayBlockly=(
                <React.Fragment>
                    <ul className='nav nav-tabs nav-fill mt-4' role="tablist">
                        <li className='nav-item' role="presentation">
                            <button type='button' className="nav-link active" data-bs-toggle="tab" data-bs-target="#blockDef">積木方塊定義</button>
                        </li>
                        <li className='nav-item' role="presentation">
                            <button type="button" className="nav-link" data-bs-toggle="tab" data-bs-target="#blockCodeDef">積木產生程式碼定義</button>
                        </li>
                    </ul>
                    <div className='tab-content border'>
                        <div className="tab-pane fade show active" id="blockDef">
                            <Editor height="20vh" defaultLanguage='json' defaultValue={JSON.stringify(util.example.blockDef,null,4)} onMount={this.handleBlockEditorDidMount}/>
                        </div>
                        <div className="tab-pane fade" id="blockCodeDef">
                            <Editor height="20vh" defaultLanguage='javascript' defaultValue={util.example.blockCodeDef} onMount={this.handleBlockDefEditorDidMount}/>
                        </div>
                    </div>
                    <div className='d-grid'>
                        <button type='button' className='btn btn-primary' onClick={this.addBlock}>新增</button>
                    </div>
                    <div className='input-group mb-3'>
                        <span className='input-group-text'>所有章節<br/>共用積木方塊</span>
                        <div className="form-control vh-60" style={{width: "500px"}} id={`shareBlock`}></div>
                    </div>
                </React.Fragment>
            );
        }


        return (
            <form className='p-4' onSubmit={this.handleSubmit}>
                <div className='input-group'>
                    <span className='input-group-text'>外部檔案連結</span>
                    <input type="text" className="form-control" placeholder='請輸入所需外部檔案之連結' value={this.state.currentLink} onChange={this.currentLinkChange}/>
                    <button type="button" className="btn btn-primary" onClick={this.addLink}>新增</button>
                </div>
                <div className='list-group mb-3'>
                    {linkItems}
                </div>
                {displayBlockly}
                <div className='input-group mb-3'>
                    <span className='input-group-text'>所有章節<br/>共用程式碼<br/>(JavaScript)</span>
                    <div className="form-control">
                        <Editor height="60vh" defaultLanguage='javascript' onMount={this.handleEditorDidMount}/>
                    </div>
                </div>
                <div className='d-grid'>
                    <button className='btn btn-primary'>送出</button>
                </div>
            </form>
        )
    }
}

export default CourseAdvancedForm;