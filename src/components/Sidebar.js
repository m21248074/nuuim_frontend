import React from 'react';
import {UserContext} from "../context/UserContext";
import util from '../util';

class Sidebar extends React.Component
{
    static contextType=UserContext;
    constructor(props)
    {
        super(props);
        this.state={
            student:
            [
                {
                    id: 'MyCourse',
                    name: '我的課程',
                    icon: 'bi-book-half'
                },
                {
                    id: 'MySetting',
                    name: "我的設定",
                    icon: 'bi-person-square'
                }
            ],
            provider:
            [
                {
                    id: 'CourseManage',
                    name: '課程管理',
                    icon: 'bi-book-half'
                }
            ],
            current: 'MyCourse'
        }
        this.handleClick=this.handleClick.bind(this);
    }
    handleClick(e)
    {
        this.props.setRender(util.components[e.target.id]);
        this.setState({current: e.target.id});
    }
    render()
    {
        return (
            <div className='col d-flex flex-column flex-shrink-0 p-3 text-white bg-dark vh-100'>
                <hr/>
                <ul className='nav nav-pills nav-fill flex-column mb-auto'>
                    {
                        this.state.student.map(key=>{
                            let active=(key.id===this.state.current)?'active':'';
                            return (
                                <li key={key.id} className='nav-item'>
                                    <button className={'nav-link text-white '+active} id={key.id} onClick={this.handleClick}>
                                        <i className={'bi '+key.icon}> </i>
                                        {key.name}
                                    </button>
                                </li>
                            )
                        })
                    }
                    <hr/>
                    {
                        this.state.provider.map(key=>{
                            let active=(key.id===this.state.current)?'active':'';
                            return (
                                <li key={key.id} className='nav-item'>
                                    <button className={'nav-link text-white '+active} id={key.id} onClick={this.handleClick}>
                                        <i className={'bi '+key.icon}> </i>
                                        {key.name}
                                    </button>
                                </li>
                            )
                        })
                    }
                </ul>
            </div>
        );
    }
}

export default Sidebar;