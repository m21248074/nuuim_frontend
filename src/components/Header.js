import React from 'react';
import { Link } from "react-router-dom";
import { HashLink } from "react-router-hash-link";
import { UserContext } from '../context/UserContext';
import util from '../util';

class Header extends React.Component
{
    constructor(props)
    {
        super(props);
        this.logoutClick=this.logoutClick.bind(this);
    }
    logoutClick(e)
    {
        e.preventDefault();
        util.logout()
            .then(json=>{
                console.log(json);
                console.log(this);
                let { setUser }=this.context;
                setUser(null);
            })
    }

    render()
    {
        let { user }=this.context;
        return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <div className="container-fluid">
                    <Link className="navbar-brand" to="/">虛實自學協助平台</Link>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                            {!user&& (
                                <React.Fragment>
                                    <li className="nav-item">
                                        <HashLink to="/#features" className='nav-link active'>
                                            系統特色
                                        </HashLink>
                                    </li>
                                    <li className="nav-item">
                                        <HashLink to="/#users" className='nav-link active'>
                                            系統使用對象
                                        </HashLink>
                                    </li>
                                </React.Fragment>
                            )}
                            {user&&(
                                <React.Fragment>
                                    <li className="nav-item">
                                        <Link to="/dashboard" className='nav-link active'>你好!{user.name}</Link>
                                    </li>
                                    <li>
                                        <a href='logout' onClick={this.logoutClick} className='nav-link active'>登出</a>
                                    </li>
                                </React.Fragment>
                            )}
                        </ul>
                    </div>
                </div>
            </nav>
        )
    }
}

Header.contextType=UserContext;

export default Header;