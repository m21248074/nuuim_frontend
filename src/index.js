import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
//import reportWebVitals from './reportWebVitals';
import routes from './routes';
import Header from './components/Header';
import Footer from './components/Footer';
import { UserContext } from './context/UserContext';
import util from './util';
import "./index.css";

import * as Blockly from 'blockly/core';
import 'blockly/blocks';
import 'blockly/javascript';
import * as locale from 'blockly/msg/zh-hant';

import { loader } from "@monaco-editor/react";

Blockly.setLocale(locale);

loader.config({
    "vs/nls":{
        availableLanguages:{
            '*': "zh-tw"
        }
    }
});

class App extends React.Component
{
    static contextType=UserContext;
    constructor(props)
    {
        super(props);
        this.state={
            user: null,
            setUser: (user)=>this.setState({user})
        }
    }
    componentDidMount()
    {
        console.log("app component did mount");
        util.getMe()
            .then(json=>{
                console.log(json);
                if(json.message==null)
                    this.setState({user: json});
                else
                    this.setState({user: null});
            })
    }
    render()
    {
        return (
            <UserContext.Provider value={this.state}>
                <BrowserRouter>
                    <Header/>
                    <Routes>
                        {
                            routes.map(
                                (route,index)=>{
                                    return (
                                        <Route key={index} path={route.path} element={route.component}></Route>
                                    )
                                }
                            )
                        }
                    </Routes>
                    <Footer/>
                </BrowserRouter>
            </UserContext.Provider>
        )
    }
}

ReactDOM.render(
    <React.StrictMode>
        <App/>
    </React.StrictMode>,document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
//reportWebVitals(console.log);
