import React from "react";
import { Navigate } from 'react-router-dom';
import util from "../util";
import "./LoginPage.css"
import { UserContext } from '../context/UserContext';

class LoginPage extends React.Component
{
    static contextType=UserContext;
    constructor(props)
    {
        super(props)
        this.loginClick=this.loginClick.bind(this);
    }
    componentDidMount()
    {
        document.title=util.getTitleByPrefix('登入');
    }

    loginClick(e)
    {
        e.preventDefault();
        let formData=new FormData(e.target);
        util.login(formData)
            .then(json=>{
                if(json.message==="success")
                {
                    util.getMe()
                    .then(me=>{
                        let { setUser }=this.context;
                        setUser(me);
                    });
                }
            });
    }

    render()
    {
        let { user } =this.context;
        if(user)
            return <Navigate to='/dashboard'/>;
        return (
            <section className="login py-5 bg-light">
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
                <div className="container">
                    <div className="row g-0 row-white">
                        <div className="col col-lg-5">
                            <img src="https://pecb.com/admin/apps/backend/uploads/images/pecb-elearning-en%281%29.jpg" width="500px" className="img-fluid" alt=""/>
                        </div>
                        <div className="col col-lg-7 text-center py-5">
                            <h1 className="animate__animated animate__heartBeat animate__infinite">歡迎回來</h1>
                            <form onSubmit={this.loginClick}>
                                <div className="row py-3 pt-5">
                                    <div className="col col-lg-10 offset-1">
                                        <input type="text" className="inp px-3" placeholder="帳號" name="account"/>
                                    </div>
                                </div>
                                <div className="row py-3">
                                    <div className="col col-lg-10 offset-1">
                                        <input type="password" className="inp px-3" placeholder="密碼" name="password"/>
                                    </div>
                                </div>
                                <div className="row py-3">
                                    <div className="col col-lg-10 offset-1">
                                        <button className="btnLogin">登入</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default LoginPage;