import React from 'react';
import { Navigate } from 'react-router-dom';
import MyCourse from '../components/MyCourse';
import Sidebar from '../components/Sidebar';
import { UserContext } from '../context/UserContext';
import util from '../util';

class DashboardPage extends React.Component
{
    static contextType=UserContext;
    constructor()
    {
        super();
        this.state={
            render: <MyCourse/>,
            setRender: (render)=>{this.setState({render})}
        };
    }
    componentDidMount()
    {
        document.title=util.title;
    }
    render()
    {
        let { user } =this.context;
        if(!user)
            return <Navigate to='/'/>;
        return (
            <React.Fragment>
                <div className='container-fluid'>
                    <div className='row'>
                        <Sidebar setRender={this.state.setRender} />
                        <div className='col-10'>
                            {this.state.render}
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default DashboardPage;